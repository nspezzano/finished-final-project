class Player:

    def __init__(self, cqc_slam, cqc_punch, cqc_knife, silenced, combo, level):
            self.__cqc_slam = cqc_slam
            self.__cqc_punch = cqc_punch
            self.__cqc_knife = cqc_knife
            self.__silenced= silenced
            self.__combo = combo
            self.__level = level

    
    def get_cqc_slam(self):
        return self.__cqc_slam

    def get_cqc_punch(self):
        return self.__cqc_punch

    def get_cqc_knife(self):
        return self.__cqc_knife

    def get_silenced(self):
        return self.__silenced

    def get_combo(self):
        return self.__combo
    
    def get_level(self):
        return self.__level
    
        
