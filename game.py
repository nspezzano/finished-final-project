import inventory
import os
import enemy
import player
import random
xPos = 0
yPos = 0
playerHealth = 500
rations = 0
playerExp = 0
level = 1
strength = 0.1
defence = 0.1
grenade = 0
x = 1
ree = 0
no2 = 0
no3 = 0
no4 = 0
arm = 0
ok = 0
sta = 0
print("              M E T A L  G E A R  S O L I D")
print("                 O U T E R  H E A V E N")
print("                    E P I S O D E  I")
print("")
print("                 Created by: Nick Spezzano")
print("")
print("Ocelot: Venom, you have a new mission, a mission against Cipher.")
print("This is a stealth mission, nobody can know you're here.")
print("We got a report from the intel team... ")
print("We have located one of Cipher's bases. It's on an island off the coast of Alaska.")
print("")
print("You need to infiltrate the base, find out whats going on there and destroy the base by any means neccessary...")
print("")
print("Kaz: Your objective right now is to head to the location [9, 3]. I'll be providing support via codec, good luck Boss.")
print("")
print("Which way will you go? Left(l), Right(r), Forward(f) or Down(d)?")
print("Type in 'i' to access your inventory.")
print("Type in 't' for a tutorial on battles.")
print("")
print("You begin on the bottom left corner of the map |_")
print("")
while x == 1:
    combo = 0
    enemyHealth = 200
    cqc_slam = 40
    cqc_punch = 40
    cqc_knife = 60
    no = 0
    no1 = 0
    stun = random.randint(0, 100)
    bleed = random.randint(0, 100)
    bleed1 = 0
    bleedDamage = 5
    jab = 5
    silenced = random.randint(50, 60)
    attack = random.randint(30,40)
    enemyFunctions = enemy.Enemy(enemyHealth, attack)
    playerFunctions = player.Player(cqc_slam, cqc_punch, cqc_knife, silenced,combo, level)
        

    
    def battle():
        
        import inventory
        import enemy
        import player
        import random
        global strength
        global defence
        global playerHealth
        global rations
        enemyHealth = 300
        keepGoing = True
        global playerExp
        exp = 50
        global level
        if level >= 10:
            exp = 20
        elif level >= 5:
            exp = 40
        combo = 0
        ammoMag = 8
        ammoTotal = 24
        print("You have encountered an enemy!")
        print("You go first.")
        print("")
        while keepGoing:
            global playerHealth
            cqc_slam = 40
            cqc_punch = 40
            cqc_knife = 60
            stun = random.randint(0, 100)
            bleed = random.randint(0, 100)
            bleed1 = 0
            bleedDamage = 5
            jab = 5
            silenced = random.randint(50, 60)
            attack = random.randint(30,40)
            enemyFunctions = enemy.Enemy(enemyHealth, attack)
            playerFunctions = player.Player(cqc_slam, cqc_punch, cqc_knife, silenced,combo, level)
            totalAttack = int(playerFunctions.get_cqc_slam()*strength)
            totalDefence = int(enemyFunctions.get_attack()*defence)
            print("It is now your turn.")
            print("Will you...")
            print("1.)Use CQC slam")
            print("2.)Use a silenced pistol. Ammo:",ammoMag,"/",ammoTotal )
            print("3.)Use CQC punch")
            print("4.)Use knife CQC ")
            print("5.)Bionic move")
            print("6.)Use an item")
            playerChoice = input("")
            if playerChoice == ("1"):
                print("You sweep the enemies legs and slam him on the ground. Does", int(playerFunctions.get_cqc_slam() + totalAttack),"damage.")
                print("")
                print("Enemy turn!")
                print("The enemy gets up and slashes at you with his knife. Does", int(enemyFunctions.get_attack() - totalDefence), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+totalDefence))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_slam()- totalAttack))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
            elif playerChoice == ("2"):
                if ammoTotal == 0 and ammoMag == 0:
                    print("You are out of ammo!")
                elif ammoMag == 0 and ammoTotal > 0:
                    ammoMag = 8
                    ammoTotal = ammoTotal - 8
                    print("You reload.")
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                elif ammoMag > 0 and ammoTotal >0:
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does",int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                elif ammoMag > 0 and ammoTotal == 0:
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                
            elif playerChoice == ("3"):
                print("You punch the enemy with multiple hits. Does", int(playerFunctions.get_cqc_punch()+ (playerFunctions.get_cqc_punch()*strength)), 'damage.')
                print("Enemy turn!")
                print("The enemy fires back at you. Does", int(enemyFunctions.get_attack() - (enemyFunctions.get_attack()*defence)), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_punch()-(playerFunctions.get_cqc_punch()*strength)))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
                                
            elif playerChoice == ("4"):
                print("You slash at the enemy with your knife. Does,", int(playerFunctions.get_cqc_knife()+(playerFunctions.get_cqc_knife()*strength)), "damage.")
                print("Enemy turn!")
                print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife()- (playerFunctions.get_cqc_knife()*strength)))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
                
            elif playerChoice == ("5"):
                if combo >= 120:
                    print("Bionic arm is charged! Choose a special move.")
                    print("1.) Rocket punch (-120 on power meter).")
                    if arm == 1:
                        print("2.) Bolt punch (-200 on power meter).")
                        ok = 1
                    bionic = input("")
                    if bionic == "1":
                        print("You shoot your bionic arm at the enemy. Does 100 damage.")
                        print("Enemy turn!")
                        print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                        playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                        print("Venom Health:", playerHealth)
                        enemyHealth = int((enemyFunctions.get_enemyHealth()- 100))
                        print("Enemy Health:", enemyHealth)
                        combo -= 120
                        print("")
                        print("Power meter:", combo)
                    elif bionic == "2" and arm == 1:
                        print("You punch the enemy and release a burst of electricity. Does 200 damage.")
                        print("Enemy turn!")
                        print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                        playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                        print("Venom Health:", playerHealth)
                        enemyHealth = int((enemyFunctions.get_enemyHealth()- 200))
                        print("Enemy Health:", enemyHealth)
                        combo -= 200
                        print("")
                    else:
                        print("")
                    
            elif playerChoice == ("6"):
                invnt2 = input("Would you like to see your health items(i) or attack items(k)?")
                if invnt2 == "I" or "i":
                    print("You have", rations,"rations.")
                    health = input("Will you use one? Y/N")
                    if health == ("y" or "Y"):
                        if rations == 0:
                            print("You don't have any rations!")
                        else:
                            print("Your health is restored by 100 points.")
                            rations -= 1
                            print(playerHealth + 100)
                            print("Enemy turn!")
                            print("The enemy fires at you. Does,", enemyFunctions.get_attack(), "damage.")
                            playerHealth = (playerHealth -  enemyFunctions.get_attack())
                            print("Venom Health:", playerHealth)
                            enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife())
                            print("Enemy Health:", enemyHealth)
                            combo = (playerFunctions.get_combo() + 40)
                            print("")
                            print("Power meter:", combo)
                    elif health == "n" or "N":
                        print("You didn't use a ration.")
                elif invnt2 == "k" or "K":
                    print("You have", grenade,"grenades.")
                    atk = input("Will you use one? Y/N")
                    if atk == ("y" or "Y"):
                        if grenade == 0:
                            print("You don't have any grenades!")
                        else:
                            print("You throw the grenade at the enemy. It explodes and does 100 damage!")
                            grenade -= 1
                            print("Enemy Health:", enemyFunctions.get_enemyHealth() - 100)
                            print("Enemy turn!")
                            print("The enemy fires at you. Does,", enemyFunctions.get_attack(), "damage.")
                            playerHealth = (playerHealth -  enemyFunctions.get_attack())
                            print("Venom Health:", playerHealth)
                            enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife())
                            print("Enemy Health:", enemyHealth)
                            combo = (playerFunctions.get_combo() + 40)
                            print("")
                            print("Power meter:", combo)
                    elif health == "n" or "N":
                        print("You didn't use a ration.")
                    
            if enemyHealth <= 0:
                keepGoing = False
                print("You have won the battle! You have gained", exp, "exp.")
                playerExp += exp
                print("Total exp:", playerExp)
                if playerExp >= 200:
                    playerExp = 0
                    print("")
                    print("Exp is set back to zero.")
                    print("Level up! Strength +5 and defence +3")
                    level += 1
                print("You are level", level)
            if playerHealth <= 0:
                keepGoing = False
                print("You have lost the battle... Restart? Y/N")
                rstrt = input("")
                if rstrt == "Y" or "y":
                    playerHealth+=400
                    battle()
                elif rstrt == "N" or"n":
                    print("You are dead...")
                    x == 2
                
                

    def battle2():
        
        import inventory
        import enemy
        import player
        import random
        global strength
        global defence
        global playerHealth
        global rations
        global ammoMag
        global ammoTotal
        enemyHealth = 300
        keepGoing = True
        global playerExp
        exp = 50
        global level
        if level >= 10:
            exp = 20
        elif level >= 5:
            exp = 40
        combo = 0
        ammoMag = 8
        ammoTotal = 24
        print("You have encountered an enemy!")
        print("Enemy goes first.")
        print("The enemy slashes at you with his knife. Does 40 damage!")
        playerHealth = playerHealth - 40
        print("Venom health:", playerHealth)
        print("")
        while keepGoing:
            cqc_slam = 40
            cqc_punch = 40
            cqc_knife = 60
            stun = random.randint(0, 100)
            bleed = random.randint(0, 100)
            bleed1 = 0
            bleedDamage = 5
            jab = 5
            silenced = random.randint(50, 60)
            attack = random.randint(30,40)
            enemyFunctions = enemy.Enemy(enemyHealth, attack)
            playerFunctions = player.Player(cqc_slam, cqc_punch, cqc_knife, silenced,combo, level)
            totalAttack = int(playerFunctions.get_cqc_slam()*strength)
            totalDefence = int(enemyFunctions.get_attack()*defence)
            print("It is now your turn.")
            print("Will you...")
            print("1.)Use CQC slam")
            print("2.)Use a silenced pistol. Ammo:",ammoMag,"/",ammoTotal )
            print("3.)Use CQC punch")
            print("4.)Use knife CQC ")
            print("5.)Bionic move")
            print("6.)Use an item")
            playerChoice = input("")
            if playerChoice == ("1"):
                print("You sweep the enemies legs and slam him on the ground. Does", int(playerFunctions.get_cqc_slam() + totalAttack),"damage.")
                print("")
                print("Enemy turn!")
                print("The enemy gets up and slashes at you with his knife. Does", int(enemyFunctions.get_attack() - totalDefence), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+totalDefence))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_slam()- totalAttack))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
            elif playerChoice == ("2"):
                if ammoTotal == 0 and ammoMag == 0:
                    print("You are out of ammo!")
                elif ammoMag == 0 and ammoTotal > 0:
                    ammoMag = 8
                    ammoTotal = ammoTotal - 8
                    print("You reload.")
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                elif ammoMag > 0 and ammoTotal >0:
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does",int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                elif ammoMag > 0 and ammoTotal == 0:
                    print("You fire 2 shots at the enemy. Does", int(playerFunctions.get_silenced()+(playerFunctions.get_silenced()*strength)), "damage.")
                    ammoMag = (ammoMag - 2)
                    print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                    playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                    print("Venom Health:", playerHealth)
                    enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced()-playerFunctions.get_silenced()*strength))
                    print("Enemy Health:", enemyHealth)
                    combo = (playerFunctions.get_combo() + 40)
                    print("")
                    print("Power meter:", combo)
                
            elif playerChoice == ("3"):
                print("You punch the enemy with multiple hits. Does", int(playerFunctions.get_cqc_punch()+ (playerFunctions.get_cqc_punch()*strength)), 'damage.')
                print("Enemy turn!")
                print("The enemy fires back at you. Does", int(enemyFunctions.get_attack() - (enemyFunctions.get_attack()*defence)), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+(enemyFunctions.get_attack()*defence)))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_punch()-(playerFunctions.get_cqc_punch()*strength)))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
                                
            elif playerChoice == ("4"):
                print("You slash at the enemy with your knife. Does,", int(playerFunctions.get_cqc_knife()+(playerFunctions.get_cqc_knife()*strength)), "damage.")
                print("Enemy turn!")
                print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                print("Venom Health:", playerHealth)
                enemyHealth = int((enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife()- (playerFunctions.get_cqc_knife()*strength)))
                print("Enemy Health:", enemyHealth)
                combo = (playerFunctions.get_combo() + 40)
                print("")
                print("Power meter:", combo)
                
            elif playerChoice == ("5"):
                if combo >= 120:
                    print("Bionic arm is charged! Choose a special move.")
                    print("1.) Rocket punch (-120 on power meter).")
                    if arm == 1:
                        print("2.) Bolt punch (-200 on power meter).")
                        ok = 1
                    bionic = input("")
                    if bionic == 1:
                        print("You shoot your bionic arm at the enemy. Does 100 damage.")
                        print("Enemy turn!")
                        print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                        playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                        print("Venom Health:", playerHealth)
                        enemyHealth = int((enemyFunctions.get_enemyHealth()- 100))
                        print("Enemy Health:", enemyHealth)
                        combo -= 120
                        print("")
                        print("Power meter:", combo)
                    elif bionic == 2 and arm == 1:
                        print("You punch the enemy and release a burst of electricity. Does 200 damage.")
                        print("Enemy turn!")
                        print("The enemy fires at you. Does", int(enemyFunctions.get_attack()-(enemyFunctions.get_attack()*defence)), "damage.")
                        playerHealth = int((playerHealth -  enemyFunctions.get_attack()+ (enemyFunctions.get_attack()*defence)))
                        print("Venom Health:", playerHealth)
                        enemyHealth = int((enemyFunctions.get_enemyHealth()- 200))
                        print("Enemy Health:", enemyHealth)
                        combo -= 200
                        print("")
                    else:
                        print("")
                else:
                    print("Combo meter is not filled up enough!")
                    
            elif playerChoice == ("6"):
                print("You have", rations,"rations.")
                health = input("Will you use one? Y/N")
                if health == ("y" or "Y"):
                    if rations == 0:
                        print("You don't have any rations!")
                    else:
                        print("Your health is restored by 100 points.")
                        rations -= 1
                        print(playerHealth + 100)
                        print("Enemy turn!")
                        print("The enemy fires at you. Does,", enemyFunctions.get_attack(), "damage.")
                        playerHealth = (playerHealth -  enemyFunctions.get_attack())
                        print("Venom Health:", playerHealth)
                        enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife())
                        print("Enemy Health:", enemyHealth)
                        combo = (playerFunctions.get_combo() + 40)
                        print("")
                        print("Power meter:", combo)
                elif health == "n" or "N":
                    print("You didn't use a ration.")
            if enemyHealth <= 0:
                keepGoing = False
                print("You have won the battle! You have gained", exp, "exp.")
                playerExp += exp
                print("Total exp:", playerExp)
                if playerExp >= 200:
                    playerExp = 0
                    print("")
                    print("Exp is set back to zero.")
                    print("Level up! Strength +5 and defence +3")
                    level += 1
                    print("You are level", level)
            if playerHealth <= 0:
                keepGoing = False
                print("You have lost the battle... Restart?")
    

                
    def sneak2():
        froggo = 2
        global strength
        global defence
        global playerExp
        global level
        chance1 = random.randint(10, 70)
        chance2 = random.randint(10, 70)
        chance3 = random.randint(10, 70)
        chance4 = random.randint(10, 70)
        a = random.randint(1, 100)
        b = random.randint(1, 100)
        c = random.randint(1, 100)
        d = random.randint(1, 100)
        e = random.randint(1, 4)
        sneakOp1 = ("You make a noise by rattling an air vent. The enemy turns around!")
        sneakOp2 = ("You make a loud footstep near the enemy. The enemy turns around!")
        sneakOp3 = ('You hold your gun up to the enemy. "Freeze!"')
        sneakOp35 = ("You crawl in the enemies line of vision. You're spotted!")
        sneakOp4 = ("The enemy turns around. You're spotted!")

        print("You see an enemy...")
        print("Will you...")
        print("")
        print("1. Try to sneak up and knock out the enemy(", chance1,"% chance.)")
        print("2. Try to sneak past(", chance2,"% chance.)")
        print("3. Distract the enemy(", chance3,"% chance.)")
        print("4. Try to hold up the enemy(", chance4,"% chance.)")
        print("5. Battle the enemy.")
        sneak = input("")
        while froggo == 2:
            if sneak == "1":
                print("You walk slowly towards the enemy...")
                if a <= chance1:
                    print("You sneak up and knock out the enemy.")
                    print("You gain 40 exp.")
                    playerExp += 40
                    print("Total exp:", playerExp)
                    froggo = False
                    if playerExp >= 200:
                        playerExp = 0
                        print("")
                        print("Exp is set back to zero.")
                        print("Level up! Strength +1 and defence +2")
                        strength += 0.1
                        defence += 0.2
                        level += 1
                else:
                    if e == 1:
                        print(sneakOp1)
                        battle2()
                        froggo = False
                        
                    elif e == 2:
                        print(sneakOp2)
                        battle2()
                        froggo = False
                        
                    elif e == 3:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        
                    elif e == 4:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        
            elif sneak == "2":
                print("You begin to crawl past the enemy...")
                if a <= chance2:
                    print("You manage to sneak past unnoticed.")
                    print("You gain 40 exp.")
                    playerExp += 40
                    print("Total exp:", playerExp)
                    froggo = False
                    if playerExp >= 200:
                        playerExp = 0
                        print("")
                        print("Exp is set back to zero.")
                        print("Level up! Strength +1 and defence +2")
                        strength += 0.1
                        defence += 0.2
                        level += 1
                else:
                        
                    if e == 1:
                        print(sneakOp1)
                        battle2()
                        froggo = False
                        
                    elif e == 2:
                        print(sneakOp2)
                        battle2()
                        froggo = False
                        
                    elif e == 3:
                        print("The enemy realizes the noise is nothing and turns around.")
                        print("You're spotted!")
                        battle2()
                        froggo = False
                        
                    elif e == 4:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        
                        
            elif sneak == "3":
                print("You throw an empty magazine away from your path. It makes a loud noise.")
                print("The enemy is suspicious of the noise and walks near the magazine.")
                if a <= chance3:
                    print("The enemy investigates the noise.")
                    print("You manage to sneak past the enemy while he's distracted.")
                    print("You gain 40 exp.")
                    playerExp += 40
                    print("Total exp:", playerExp)
                    froggo = False
                    if playerExp >= 200:
                        playerExp = 0
                        print("")
                        print("Exp is set back to zero.")
                        print("Level up! Strength +1 and defence +2")
                        strength += 0.1
                        defence += 0.2
                        level += 1
                else:
                    
                    if e == 1:
                        print(sneakOp1)
                        battle2()
                        froggo = False
                        
                    elif e == 2:
                        print(sneakOp2)
                        battle2()
                        froggo = False
                        
                    elif e == 3:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        
                    elif e == 4:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        

            elif sneak == "4":
                print("You walk towards the enemy, gun in hand.")
                if a <= chance3:
                    print(sneakOp3)
                    print("The enemy drops his gun and lays on the floor with his hands behind his back.")
                    print("You gain 40 exp.")
                    playerExp += 40
                    print("Total exp:", playerExp)
                    froggo = False
                    if playerExp >= 200:
                        playerExp = 0
                        print("")
                        print("Exp is set back to zero.")
                        print("Level up! Strength +1 and defence +2")
                        strength += 0.1
                        defence += 0.2
                        level += 1
                else:
                    
                    if e == 1:
                        print(sneakOp1)
                        battle2()
                        froggo = False
                        
                    elif e == 2:
                        print(sneakOp2)
                        battle2()
                        froggo = False
                        
                    elif e == 3:
                        print(sneakOp3)
                        print("The enemy turns around and slashes with his knife!")
                        battle2()
                        froggo = False
                        
                    elif e == 4:
                        print(sneakOp4)
                        battle2()
                        froggo = False
                        

            elif sneak == "5":
                battle()
                froggo = False

            elif sneak == "6":
                print("")
                froggo = False
            else:
                sneak = input("")
    def b3():
        import inventory
        import os
        import enemy
        import player
        import random
        global rations
        global grenade
        xPos = 0
        yPos = 0
        k = 0
        dry = 1
        print("Kaz: Great Boss, you've reached the third floor basement. This is the armory, perhaps you can find some weapondry and equipment.")
        print("Get to the objective at location [6,3]")
        print("You begin on the bottom left corner of the map |_")
        print("")
        print("------B3 Armory------")
        print("")
        while dry == 1:
            pos = [xPos, yPos]
            u = input("")
            if u == ("l" or "L"):
                print(u)
                xPos -= 1
                print("Location[x, y]:",pos)
                if xPos < 0:
                    print("There is a wall there.")
                    xPos = xPos+1
            elif u == ("r" or "R"):
                print(u)
                xPos += 1
                print("Location[x, y]:",pos)
                if xPos > 6:
                    print("There is a wall there.")
                    xPos -=1
            elif u == ("f" or "F"):
                print(u)
                yPos +=1
                print("Location[x, y]:",pos)
                if yPos > 3:
                    print("There is a wall there.")
                    yPos -=1
                
            elif u == ("d" or "D"):
                print(u)
                yPos -=1
                print("Location[x, y]:",pos)
                if yPos < 0:
                    print("There is a wall there.")
                    yPos +=1
            if pos == [0, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                if k == 0:
                    print("Kaz: The armory is better guarded than the first floor, expect higher level soldiers.")
                    global k
                    k+=1
                    print("")
                    print("There's a room in front of you.")
                    print("You found a grenade.")
                    grenade+=1
               
            elif pos == [1, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("There is a room in front of you.")
                print("There are 3 rations inside.")
                rations += 3
                          
            elif pos == [2, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")

            elif pos == [3, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()

            elif pos == [1, 1]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak()
                print("")
                
            elif pos == [2, 1]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [3, 1]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [1, 2]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [2, 2]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [6, 3]:
                print("You enter a big room with seats and a big computer screen.")
                print("The screen flashes on.")
                print("???: I have been waiting for you, Big Boss.")
                print("Venom: Huh? Who are you?")
                print("???: You can call me Xavier, second in command of Cipher. I am like you, a phantom without a past.")
                print("???: You've been poking around in Cipher's buisiness, and for that I can't let you live.")
                print("There is a pod to your left and to your right. They slowly open.")
                print("Two figures walk out, figures that look very similar to you...")
                print("The room fills with gas.")
                print("Everything slowly turns black...")
                print("End of Episode I.")



                    
            elif pos == [4, 1]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [4, 2]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [4, 3]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [5, 1]:
                print("There is a room. You go inside.")
                print("There are no items in this room, all there is are pictures... pictures of you.")


            elif pos == [5, 2]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")

            elif pos == [5, 3]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
    
            elif pos == [1, 3]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [3, 2]:
                print("")
      
                
            elif pos == [6, 2]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")
                
            elif pos == [6, 1]:
                print()



            elif pos == [4, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                print("")

            elif pos == [5, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
                
            elif pos == [6, 0]:
                n = random.randint(0, 20)
                if n >= 10:
                    print("")
                if n < 10:
                    sneak2()
            

    pos = [xPos, yPos]
    u = input("")
    if u == ("t" or "T"):
        import battleTutorial
        
    elif u == ("i" or "I"):
        invnt = input("Would you like to see your health items(i), weapons(w), or attack items(k)?")
        if invnt == ("i" or "I"):
            print("You have", rations,"rations.")
            health = input("Will you use one? Y/N")
            if health == ("y" or "Y"):
                if rations == 0:
                    print("You don't have any rations!")
                else:
                    print("Your health is restored by 100 points.")
                    rations -= 1
                    playerHealth+=100       
                    print(playerHealth)
            elif health == "n" or "N":
                print("You didn't use a ration.")
                
    elif u == ("l" or "L"):
        xPos = xPos - 1
        if xPos < 0:
            print("There is a wall there.")
            xPos = xPos+1
        print("Location[x, y]:",pos)
    elif u == ("r" or "R"):
        xPos = xPos + 1
        if xPos > 9:
            print("There is a wall there.")
            xPos = xPos - 1
        print("Location[x, y]:",pos)
    elif u == ("f" or "F"):
        yPos = yPos+ 1
        if yPos > 3:
            print("There is a wall there.")
            yPos = yPos - 1
        print("Location[x, y]:",pos)
    elif u == ("d" or "D"):
        yPos = yPos - 1
        if yPos < 0:
            print("There is a wall there.")
            yPos = yPos + 1
        print("Location[x, y]:",pos)       

    pos = [xPos, yPos]

    if pos == [0, 0]:
        if ree == 0:
            print("Kaz: Great you've entered the base... Security doesn't look too bad. Find out what's going on here, boss.")
            ree+=1
        else:
            print("The room is lightly guarded... odd.")
      
        
    elif pos == [1, 0]:
            n = random.randint(0, 20)
            if n >= 10:
                print("")
            elif n < 10:
                sneak2()
            print("")
                     
                     
        
    elif pos == [2, 0]:

            n = random.randint(0, 20)
            if n >= 10:
                print("")
            elif n < 10:
                sneak2()
            if no == 0:              
                print("There is a room to your left. You go inside.")
                print("There are 3 rations.")
                rations += 3
                no+=1
            
    elif pos == [-1, 0]:
        print("There is a wall.")
        pos = [0,0]
    elif pos == [3, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        elif n < 10:
            sneak2()
            
    elif pos == [1, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        elif n < 10:
            sneak2()
        print("")
        
    elif pos == [2, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("There are tanks everywhere. Looks like they're preparing for war.")
        
    elif pos == [3, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [1, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no1 == 0:
            print("You found a grenade.")
            grenade += 1
            no1+=1
        print("")
        
    elif pos == [2, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [3, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [1, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [2, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no3 == 0:              
            print("There is a room to your left. You go inside.")
            print("There are 3 rations.")
            rations += 3
            no+=1
        print("")
        
    elif pos == [3, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [0, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
               
    elif pos == [0, 2]:
        print("")
        
    elif pos == [0, 3]:
        print("")

      
        
    elif pos == [1, 0]:
            n = random.randint(0, 20)
            if n >= 10:
                print("")
            if n < 10:
                sneak2()
            print("")
                     
                     
        
    elif pos == [2, 0]:

        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no == 0:              
            print("There is a room to your left. You go inside.")
            print("There are 3 rations.")
            rations += 3
            no+=1
            

    elif pos == [4, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
            
    elif pos == [5, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [6, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [7, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        print("There are missiles outside. Enough to blow up an entire city.")
        
    elif pos == [8, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no1 == 0:
            print("You found a grenade.")
            grenade += 1
            no1+=1
        print("")
        
    elif pos == [9, 0]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        print("There's a big window that looks into somebody's lab.")
        print("There are pictures of you before and after the downfall of MSF.")
        print("No... there's another. There is one of you with")
        print("the shrapnel in your head and with your real arm. Odd.")
        print("Kaz: Uh, Boss that's probably nothing. Move on.")
        
    elif pos == [4, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [4, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no2 == 0:              
            print("There is a room to your left. You go inside.")
            print("There are 3 rations.")
            rations += 3
            no+=1
        print("")
     
        
    elif pos == [4, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no4 == 0:
            print("There's a room in front of you. You go in.")
            print("Kaz: That looks like data for a new bionic move.")
            print("Go ahead and download it to your arm.")
            arm +=1
            no4 += 1
    
    elif pos == [5, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [5, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
               
    elif pos == [5, 3]:
        print("")
        
    elif pos == [6, 1]:
        print("")

    elif pos == [6, 2]:
       n = random.randint(0, 20)
       if n >= 10:
           print("")
       if n < 10:
           sneak2()
       print("")
      
        
    elif pos == [6, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
                     
                     
        
    elif pos == [7, 1]:

        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no == 0:              
            print("There is a room to your left. You go inside.")
            print("There are 3 rations.")
            rations += 3
            no+=1
            
    elif pos == [7, 2]:
        print("There is a wall.")
        pos = [0,0]
        
    elif pos == [7, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
            
    elif pos == [8, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [8, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [8, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [9, 1]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        if no1 == 0:
            print("You found a grenade.")
            grenade += 1
            no1+=1
        print("")
        
    elif pos == [9, 2]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("")
        
    elif pos == [9, 3]:
        n = random.randint(0, 20)
        if n >= 10:
            print("")
        if n < 10:
            sneak2()
        print("Kaz: Great you've reached the objective point. Huh? Boss, get to cover! I hear footsteps!")
        print("???: What is the status on the project?")
        print("Soldier: Just fine. A few dead, but they are necessary sacrifices.")
        print("Soldier: A weapon to rival the legendary mercenary...")
        print("???: Stop. We'll talk about this later. Report to the armory in underground level 3.")
        print("???: Come take the stairs with me, there is one more thing I have to tell you.")
        print("Kaz: A weapon to rival the legendary mercenary... That's you Boss!")
        print("Kaz: Follow that soldier to underground level 3 and find out what's going on here.")
        print("")
        b3()
        
            
            
