class Inventory:

    def __init__(self, key_items, rations, items, weapons):
        self.__key_items = key_items
        self.__items = items
        self.__weapons = weapons
        self.__rations = rations

    def get_key_items(self):
        return self.__key_items
    def get_items(self):
        return self.__items
    def get_weapons(self):
        return self.__weapons
    def get_rations(self):
        return self.__rations
    
