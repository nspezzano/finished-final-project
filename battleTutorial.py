import inventory
import enemy
import player
import random

playerHealth = 500
enemyHealth = 20
keepGoing = True
exp = 40
x = 0
level = 1
if level >= 10:
    exp = 10
elif level >= 5:
    exp = 20
combo = 0
ammoMag = 8
ammoTotal = 24
print("You have encountered an enemy!")
print("You go first.")
print("")
while keepGoing == True:
    cqc_slam = 40
    cqc_punch = 40
    cqc_knife = 60
    stun = random.randint(0, 100)
    bleed = random.randint(0, 100)
    bleed1 = 0
    bleedDamage = 5
    jab = 5
    silenced = random.randint(50, 60)
    attack = random.randint(30,40)
    enemyFunctions = enemy.Enemy(enemyHealth, attack)
    playerFunctions = player.Player(playerHealth, cqc_slam, cqc_punch, cqc_knife, silenced,combo, exp, level)
    print("It is now your turn.")
    print("")
    if x == 0:
        print("Alright boss, these are the moves you can use, the cqc moves are your basic attacks, the other moves have a discription next to them. Choose one.")
        global x
        x+=1
    elif x ==1:
        print("That's the legendary mercenary I know! I didn't think you'd need my help anyway, you can take it from here.")
        global x
        x+=1
    print("Will you...")
    print("1.)Use CQC slam")
    print("2.)Use a silenced pistol. Ammo:",ammoMag,"/",ammoTotal, "(You can equip any weapon here. You can see how much ammo you have on the right. You can get more by defeating enemies and finding ammo throughout the map.)")
    print("3.)Use CQC punch")
    print("4.)Use knife CQC ")
    print("5.)Bionic move. (These moves charge up throughout the battle. You can unlock more bionic moves by leveling up.)")
    print("6.)Use an item. (items can be found throughout the map. Each differant item has a differant use.)")
    playerChoice = input("")
    if playerChoice == ("1"):
        print("You sweep the enemies legs and slam him on the ground. Does 40 damage.")
        print("")
        print("Enemy turn!")
        print("The enemy gets up and slashes at you with his knife. Does", enemyFunctions.get_attack(), "damage.")
        playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
        print("Player Health:", playerHealth)
        enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_slam())
        print("Enemy Health:",enemyHealth)
        combo = (playerFunctions.get_combo() + 40)
        print("")
        print("Power meter:", combo)
    elif playerChoice == ("2"):
        if ammoTotal == 0 and ammoMag == 0:
            print("You are out of ammo!")
        elif ammoMag == 0 and ammoTotal > 0:
            ammoMag = 8
            ammoTotal = ammoTotal - 8
            print("You reload.")
            print("You fire 2 shots at the enemy. Does", playerFunctions.get_silenced(), "damage.")
            ammoMag = (ammoMag - 2)
            print("The enemy fires at you. Does", enemyFunctions.get_attack(), "damage.")
            playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
            print("Player Health:", playerHealth)
            enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced())
            print("Enemy Health:",enemyHealth)
            combo = (playerFunctions.get_combo() + 40)
            print("")
            print("Power meter:", combo)
        elif ammoMag > 0 and ammoTotal >0:
            print("You fire 2 shots at the enemy. Does", playerFunctions.get_silenced(), "damage.")
            ammoMag = (ammoMag - 2)
            print("The enemy fires at you. Does", enemyFunctions.get_attack(), "damage.")
            playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
            print("Player Health:", playerHealth)
            enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced())
            print("Enemy Health:",enemyHealth)
            combo = (playerFunctions.get_combo() + 40)
            print("")
            print("Power meter:", combo)
        elif ammoMag > 0 and ammoTotal == 0:
            print("You fire 2 shots at the enemy. Does", playerFunctions.get_silenced(), "damage.")
            ammoMag = (ammoMag - 2)
            print("The enemy fires at you. Does", enemyFunctions.get_attack(), "damage.")
            playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
            print("Player Health:", playerHealth)
            enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_silenced())
            print("Enemy Health:",enemyHealth)
            combo = (playerFunctions.get_combo() + 40)
            print("")
            print("Power meter:", combo)
        
    elif playerChoice == ("3"):
        print("You punch the enemy with multiple hits. Does,", playerFunctions.get_cqc_punch(), "damage.")
        print("Enemy turn!")
        print("The enemy fires back at you. Does,", enemyFunctions.get_attack(), "damage.")
        playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
        print("Player Health:", playerHealth)
        enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_punch())
        print("Enemy Health:",enemyHealth)
        combo = (playerFunctions.get_combo() + 40)
        print("")
        print("Power meter:", combo)
    elif playerChoice == ("4"):
        print("You slash at the enemy with your knife. Does,", playerFunctions.get_cqc_knife(), "damage.")
        print("Enemy turn!")
        print("The enemy fires at you. Does,", enemyFunctions.get_attack(), "damage.")
        playerHealth = (playerFunctions.get_playerHealth() -  enemyFunctions.get_attack())
        print("Player Health:", playerHealth)
        enemyHealth = (enemyFunctions.get_enemyHealth()- playerFunctions.get_cqc_knife())
        print("Enemy Health:",enemyHealth)
        combo = (playerFunctions.get_combo() + 40)
        print("")
        print("Power meter:", combo)
        
    elif playerChoice == ("5"):
        if combo >= 120:
            print("Bionic arm is charged! Choose a special move.")
            print("1.) Power punch (-80 on power meter).")
            input("")
        else:
            print("Combo meter is not filled up enough!")
    elif playerChoice == ("6"):
        print("")
        
        
    if enemyHealth <= 0:
        keepGoing = False
        print("You have won the battle! You have gained", exp, "exp.")
        playerExp = (playerFunctions.get_exp() + exp)
        if playerExp >= 100:
            print("Level up! Strength +5 and defence +3")
            level = level + 1
            print("You are level", level)
    if playerHealth <= 0:
        keepGoing = False
        print("You have lost the battle... Restart?")
    


